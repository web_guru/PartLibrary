import React, { Component } from "react";
import PropTypes from "prop-types";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import { Link } from "react-router-dom";
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { connect } from "react-redux";
import { sendRecoveryMail } from "../../actions/actions";
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" to="/">
        Partlibrary
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const muiStyles = {
  paper: {
    marginTop: 64,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: 8,
    backgroundColor: '#dc004e',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: 8,
  },
  submit: {
    marginBottom: 16,
  },
};

class ForgotPassword extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      errors: {},
      buttonDisable: false
    };
  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        open: true,
        buttonDisable: false,
        errors: nextProps.errors
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.sendRecoveryMail({email: this.state.email});
    this.setState({buttonDisable: true});
  };

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({open: false});
  };

  render() {
    const { errors, open, buttonDisable } = this.state;
    const errorMessage = Object.values(errors)[0];
    console.log(errors);

    return (
      <Container component="main" maxWidth="xs">
        { errors.noSendRecoveryMailError &&
          <Snackbar open={open} autoHideDuration={2000} onClose={this.handleClose}
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
            <Alert severity="success">
              Recovery Eamil was sent to your mailbox.
            </Alert>
          </Snackbar>
        }
        { !errors.noSendRecoveryMailError &&
          <Snackbar open={open} autoHideDuration={2000} onClose={this.handleClose}
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
            <Alert severity="error">
              {errorMessage}
            </Alert>
          </Snackbar>
        }
        <CssBaseline />
        <div style={muiStyles.paper}>
          <Avatar style={muiStyles.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Reset Password
        </Typography>
          <form style={muiStyles.form} noValidate onSubmit={this.onSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={this.onChange}
              value={this.state.email}
              error={errors.email}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              style={muiStyles.submit}
              disabled={buttonDisable}
            >
              Reset Password
          </Button>
          </form>
        </div>
        <Box mt={8}>
          <Copyright />
        </Box>
      </Container>
    );
  }
}

ForgotPassword.propTypes = {
  sendRecoveryMail: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { sendRecoveryMail }
)(ForgotPassword);
