import React, { useEffect } from 'react';
import { connect } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { positions } from '@material-ui/system';
import { setPartsApp } from "../../actions/actions";



const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(2),
      width: theme.spacing(25),
      height: theme.spacing(25),
    },
  },
  button: {
    width: theme.spacing(25),
    height: theme.spacing(25),
    fontSize: 20
  },
  link: {
    textDecoration: 'none',
    color: 'black',
  },
  menu: {
    marginLeft: '5%',
    marginTop: 10
  }
}));

function SimplePaper(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [rows, setRows] = React.useState([]);

  useEffect(() => {
    fetch('/api/data/partapp')
      .then(results => results.json())
      .then(data => {
        console.log(data);
        const result = data.filter(element => element.active === true);
        setRows(result);
      });
  }, []); //
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleMenuClose = (element) => {
    setAnchorEl(null);
    props.setPartsApp(element);
  }
  const classes = useStyles();
  const { parts } = props;
  return (
    <div className={classes.root}>
      <Paper elevation={5}>
        <Button className={classes.button} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} disabled={rows.length == 0}
        >
          Create
          </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          className={classes.menu}
        >
          {rows &&
            rows.map(element => (
              <Link to="/creator" className={classes.link} key={Math.random()}>
                <MenuItem key={element.name} onClick={() => handleMenuClose(element)}>{element.name}</MenuItem>
              </Link>
            ))
          }
        </Menu>
      </Paper>
      {parts && parts[2] &&
        <Paper elevation={5}>
          <Link to={`/creator/${parts[2]._id}/${parts[2].name}`} className={classes.link}>
            <Button className={classes.button}>
              {parts[2].name}
            </Button>
          </Link>
        </Paper>
      }
      {parts && parts[1] &&
        <Paper elevation={5}>
          <Link to={`/creator/${parts[1]._id}/${parts[1].name}`} className={classes.link}>
            <Button className={classes.button}>
              {parts[1].name}
            </Button>
          </Link>
        </Paper>
      }
      {parts && parts[0] &&
        <Paper elevation={5}>
          <Link to={`/creator/${parts[0]._id}/${parts[0].name}`} className={classes.link}>
            <Button className={classes.button}>
              {parts[0].name}
            </Button>
          </Link>
        </Paper>
      }
    </div>
  );
}

const mapStateToProps = state => ({
  partApp: state.partApp,
  parts: state.parts
});

export default connect(
  mapStateToProps,
  { setPartsApp }
)(SimplePaper);