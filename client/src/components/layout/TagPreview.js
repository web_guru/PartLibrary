import React, { Component } from "react";
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const muiStyles = {
  loadButton: {
    marginTop: 40,
    marginBottom: 'auto'
  },
  controlStyle: {
    margin: '0 auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
  typeSelect: {
    minWidth: 200,
    marginTop: 10,
    marginBottom: 10
  },
  tag: {
    height: 40,
    margin: 10,
  },
  saveForm: {
    marginTop: 40
  },
  confirmButton: {
    marginTop: 20,
    marginLeft: '20%'
  }
};

const SelectControl = ({
  title,
  valueList,
  width,
  selectedValue,
  onChangeValue,
}) => (
    <FormControl style={muiStyles.typeSelect}>
      <FormControlLabel
        style={{ marginRight: 0, whiteSpace: 'nowrap' }}
        control={
          <Select
            onChange={({ target: { value } }) => onChangeValue(value)}
            value={selectedValue}
            input={<OutlinedInput style={{ height: 35, width, ...muiStyles.tag }} />}
          >
            {valueList &&
              valueList.map((item, index) => (
                <MenuItem key={`menu-${index}`} value={item}>
                  {item}
                </MenuItem>
              ))}
          </Select>
        }
        label={title}
        labelPlacement="start"
      />

    </FormControl>
  );

const InputTag = ({ title, value, onChangeValue }) => {

  return (
    <FormControl style={muiStyles.formControl}>
      <FormControlLabel
        style={{ marginRight: 0, whiteSpace: 'nowrap' }}
        control={
          <OutlinedInput
            style={{ ...muiStyles.tag }}
            value={value}
            onChange={({ target: { value } }) => onChangeValue(value)}
          />
        }
        label={title}
        labelPlacement="start"
      />
    </FormControl>
  );
}


export default class TagPreview extends Component {

  constructor(props) {
    super(props);
    this.state = {
      partTypes: [],
      currentPartType: '',
      currentPart: null,
      currentParam: ''
    };
  }
  async componentDidMount() {
    console.log(this.props);
    const { partAppData } = this.props;
    this.setPartAppData(partAppData);
  }
  componentWillReceiveProps(nextProps) {
    const { partAppData } = nextProps;
    this.setPartAppData(partAppData);
  }
  displayData(data) {

  }

  setPartAppData(partAppData) {
    let partTyesArray = [];
    partAppData.map(element => {
      partTyesArray.push(...Object.keys(element));
    });
    this.setState({ partTypes: partTyesArray });
    this.setState({ currentPartType: partTyesArray[0] });
    this.setState({ currentPart: partAppData[0] });
    console.log(partAppData);
  }

  changeCurrentPartType(value) {
    this.setState({
      currentPartType: value,
      currentParam: ''
    });
    const { partAppData } = this.props;
    const { partTypes } = this.state
    partTypes.map((partType, index) => {
      if (!!partType.includes(value)) {
        this.setState({ currentPart: partAppData[index] })
      }
    });
  }
  changeCurrentParam(value) {
    this.setState({ currentParam: value });
  }
  render() {
    const { partTypes, currentPartType, currentPart, currentParam } = this.state;
    return (
      <Grid container item xs={12} style={{ borderStyle: 'groove' }}>
        <Grid item xs={10} style={muiStyles.controlStyle}>
          <SelectControl
            width={223}
            selectedValue={currentPartType}
            valueList={partTypes}
            onChangeValue={value => this.changeCurrentPartType(value)}>
          </SelectControl>
          <Grid item xs={12}>
            {
              currentPart && currentPart[currentPartType].Name &&
              <InputTag
                title="Name"
                value={currentPart[currentPartType].Name}
                onChangeValue={value => this.displayData(partTypes)}
              />
            }
            {
              !currentPart &&
              <InputTag
                title="Name"
                value={''}
                onChangeValue={value => this.displayData(partTypes)}
              />
            }
          </Grid>
          <Grid item xs={12}>
            {
              currentPart && currentPart[currentPartType].Param &&
              <SelectControl
                title="Param"
                selectedValue={currentParam || currentPart[currentPartType].Param[0]}
                valueList={[...currentPart[currentPartType].Param]}
                width={223}
                onChangeValue={value => this.changeCurrentParam(value)}>
              </SelectControl>
            }
            {
              !currentPart &&
              <SelectControl
                title="Param"
                selectedValue={''}
                valueList={[]}
                width={223}
                onChangeValue={value => this.displayData(partTypes)}>
              </SelectControl>
            }
          </Grid>
          <Grid item xs={12}>
            {
              currentPart && currentPart[currentPartType].Machine &&
              <InputTag
                title="Machine"
                value={currentPart[currentPartType].Machine}
                onChangeValue={value => this.displayData(partTypes)}
              />
            }
            {
              !currentPart &&
              <InputTag
                title="Machine"
                value={''}
                onChangeValue={value => this.displayData(partTypes)}
              />
            }
          </Grid>
        </Grid>
      </Grid>
    );
  }
}
