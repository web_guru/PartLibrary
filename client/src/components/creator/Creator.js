import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Render from "./Render";



class Creator extends Component {

  changePane = (pane) => {
    this.setState({
      pane
    })
  }

  constructor(props) {
    super(props);
    this.state = {
      pane: 'import',
      appVersion: ''
    };
  }
  myCallback = (dataFromChild) => {
    this.setState({ appVersion: dataFromChild });
  }

  render() {
    const { id, name } = this.props.match.params;
    const { appVersion } = this.state;
    const { partApp } = this.props;
    if (!id && (!partApp.name) && (appVersion.length === 0)) {
      return <Redirect to='/dashboard' />
    }
    return (
      <Grid container>
        { //!Object.keys(this.props.auth).includes('data') &&
          <Grid item xs={11}>
            <Render pane={this.state.pane}
              partId={id}
              partName={name}
              callbackFromParent={this.myCallback}>
            </Render>
          </Grid>
        }

        <Grid container item xs={1}>
          <Grid item xs={12} >

            <Button id='import' variant="outlined" component="span" style={{ width: '100%' }}
              onClick={() => this.changePane('import')}>
              Import STLS
            </Button>
            <Button id='tag' variant="outlined" component="span" style={{ width: '100%' }}
              onClick={() => this.changePane('tag')}>
              Tags
            </Button>
            <Button id='save' variant="outlined" component="span" style={{ width: '100%' }}
              onClick={() => this.changePane('save')}>
              Save
            </Button>
            <Button id='appVersion' variant="outlined" component="span" style={{ width: '100%', textTransform: 'none' }}
              disabled>
              App Version: {appVersion || partApp.name}
            </Button>
          </Grid>

        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = state => ({
  partApp: state.partApp
});

export default connect(
  mapStateToProps,
)(Creator);