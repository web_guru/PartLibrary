import React, { Component } from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import CircularProgress from '@material-ui/core/CircularProgress';
import TagPreview from '../layout/TagPreview';


const muiStyles = {
  loadButton: {
    marginTop: 40,
    marginBottom: 'auto'
  },
  controlStyle: {
    margin: '0 auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
  typeSelect: {
    minWidth: 200,
    marginTop: 10,
    marginBottom: 10
  },
  tag: {
    height: 40,
    margin: 10,
  },
  saveForm: {
    marginTop: 40
  },
  confirmButton: {
    marginTop: 20,
    marginLeft: '20%'
  },
  blankGrid: {
    borderStyle: 'groove',
    height: 280
  },
  loadingIcon: {
    marginLeft: '32vw',
    marginTop: '43vh',
    position: 'absolute',
  }
};

class PartsAppAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      partAppData: null,
      partAppName: null,
      adminPass: null,
      isLoading: false
    };
  }
  async componentDidMount() {
    console.log(this.props.auth);
  }


  displayData(data) {
    this.setState({ data });
  }

  setPartAppData(partAppData) {
    this.setState({ partAppData });
  }
  changePartAppName(event) {
    this.setState({ partAppName: event.target.value });
  }
  changePassword(event) {
    this.setState({ adminPass: event.target.value });
  }
  savePartApp() {
    const { partAppName, adminPass, partAppData } = this.state;
    const { user } = this.props.auth;
    const data = {
      partAppName: partAppName,
      email: user.email,
      adminPass: adminPass,
      partAppData: partAppData
    };
    console.log(data);
    this.setState({ isLoading: true });

    axios
      .post('/api/data/createpartapp', data)
      .then(res => {
        console.log(res.data);
        const { error } = res.data;
        if (error) {
          window.alert(error);
        } else {
          window.alert('Data Saved Successfully');
        }
        this.setState({ isLoading: false });
      })
      .catch(err => {
        this.setState({ isLoading: false });
        window.alert('Error occurs while saving data')
      }
      );

  }
  handleFileChange(event) {
    let files = event.target.files;
    if (!files.length) {
      alert('No file select');
      return;
    }
    let file = files[0];
    let that = this;
    let reader = new FileReader();
    reader.onload = function (e) {
      that.displayData(e.target.result);
      that.setPartAppData(JSON.parse(e.target.result));
    };
    reader.readAsText(file);
  }

  render() {
    const { data, partAppData, adminPass, partAppName, isLoading } = this.state;
    const { user } = this.props.auth;
    return (
      <Grid container style={{ height: '93vh' }}>
        {isLoading &&
          <CircularProgress style={muiStyles.loadingIcon} size={80} />
        }
        <Grid item xs={12}>
          <input
            hidden
            accept=".json"
            id="outlined-button-file"
            name="file"
            type="file"
            onChange={(e) => this.handleFileChange(e)}
          />
          <label htmlFor="outlined-button-file">
            <Button
              variant="contained"
              color="default"
              component="span"
              style={muiStyles.loadButton}
              startIcon={<CloudUploadIcon />}
            >
              Load Part App Configuration(.json)
          </Button>
          </label>
        </Grid>
        <Grid item xs={5}>
          <Typography variant="h6" component="h6">
            Text Preview
            </Typography>
          <TextField
            id="outlined-multiline-static"
            type="text"
            fullWidth
            defaultValue={data}
            multiline
            rows={35}
            variant="outlined"
            disabled
          >
          </TextField>
        </Grid>
        <Grid container item xs={7}>
          <Grid item xs={6} style={muiStyles.controlStyle}>
            <Typography variant="h6" component="h6">
              Tag Preview
            </Typography>
            {!partAppData &&
              <Grid container item xs={12} style={muiStyles.blankGrid}>
              </Grid>
            }
            {partAppData &&
              <TagPreview partAppData={partAppData}></TagPreview>
            }
            <Grid item xs={12}>
              <FormControl style={muiStyles.formControl}>
                <FormControlLabel
                  style={{ marginRight: 0, whiteSpace: 'nowrap' }}
                  control={
                    <OutlinedInput
                      id="outlined-partAppName-input"
                      type="texts"
                      style={muiStyles.tag}
                      variant="outlined"
                      required
                      onChange={event => this.changePartAppName(event)}
                    />
                  }
                  label={'PartsApp Name'}
                  labelPlacement="start"
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} style={muiStyles.saveForm}>
              <Typography variant="h6" component="h6">
                Admin User: {user.email}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <FormControl style={muiStyles.formControl}>
                <FormControlLabel
                  style={{ marginRight: 0, whiteSpace: 'nowrap' }}
                  control={
                    <OutlinedInput
                      id="outlined-password-input"
                      type="password"
                      style={muiStyles.tag}
                      autoComplete="current-password"
                      variant="outlined"
                      required
                      onChange={event => this.changePassword(event)}
                    />
                  }
                  label={'Password'}
                  labelPlacement="start"
                />
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <Button variant="outlined" color="primary" style={muiStyles.confirmButton}
                disabled={!adminPass || !partAppName || !partAppData}
                onClick={() => this.savePartApp()}
              >
                Confirm and Save
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

PartsAppAdd.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
)(PartsAppAdd);
