import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import partReducer from "./partReducer";
import partAppReducer from "./partAppReducer";

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  parts: partReducer,
  partApp: partAppReducer
});
