import { SET_PARTS_APP } from "../actions/types";

const initialState = {};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_PARTS_APP:
      return action.payload;
    default:
      return state;
  }
}
