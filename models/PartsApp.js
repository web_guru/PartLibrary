const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const PartsAppSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  partsAppData: {
    type: Array
  },
  active: {
    type: Boolean,
    default: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Part = mongoose.model("partsapp", PartsAppSchema);
