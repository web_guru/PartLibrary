const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const PartSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  userId: {
    type: String,
    required: true
  },
  meshes: [{
    type: Schema.Types.ObjectId,
    ref: "stls"
  }],
  STL_Count: {
    type: Number
  },
  x: {
    type: Array,
    required: false
  },
  y: {
    type: Array,
    required: false
  },
  z: {
    type: Array,
    required: false
  },
  tags: {
    type: Array,
    required: false
  },
  partsAppName: {
    type: String,
    required: true
  },
  partsAppData: {
    type: Array,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Part = mongoose.model("parts", PartSchema);
