const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const StlSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  azureURL: {
    type: String,
    required: true
  },
  thumbnail: {
    type: String
  },
  position: {
    x: Number,
    y: Number,
    z: Number
  },
  md5: String,
  partType: String,
  partId: {
    type: Array,
    default: []
  }
});

module.exports = Stl = mongoose.model("stls", StlSchema);
