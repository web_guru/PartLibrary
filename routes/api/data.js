
require('dotenv').config();

const express = require("express");
const router = express.Router();
const Part = require("../../models/Part");
const Stl = require("../../models/Stl");
const PartsApp = require("../../models/PartsApp");

const bcrypt = require("bcryptjs");

const azureStorage = require('azure-storage');
const blobService = azureStorage.createBlobService();


// make different Blob name when upload files with same name.
const getBlobName = originalName => {
  const identifier = Math.random().toString().replace(/0\./, ''); // remove "0." from start of string
  return `${identifier}-${originalName}`;
};


router.post("/save", async (req, res) => {
  const { partName, userId, meshes, partsAppName, partsAppData, tags } = req.body;
  let partId;
  let partX = [];
  let partY = [];
  let partZ = [];
  let existingStlIndex = [];
  meshes.map((mesh, index) => {
    partX.push(mesh.position.x);
    partY.push(mesh.position.y);
    partZ.push(mesh.position.z);
  });
  const newPart = new Part({
    name: partName,
    userId,
    partsAppName,
    partsAppData,
    tags: tags,
    STL_Count: meshes.length,
    x: partX,
    y: partY,
    z: partZ,
    meshes: []
  });
  await newPart.save()
    .then(user => {
      console.log(user);
      partId = user._id;
      res.json(user);
    })
    .catch(err => console.log(err));
  await meshes.map((mesh, index) => {
    Stl.findOneAndUpdate({ md5: mesh.md5 },
      { $push: { partId: partId } }, {
      new: true
    }, (err, result) => {
      if (result) {
        console.log('exisitng md5 file');
        console.log(result);
        existingStlIndex.push(index);
        Part.findByIdAndUpdate(
          { _id: partId },
          { $push: { meshes: result._id } },
          (err, part) => {
            if (err) {
              console.log(err)
            } else {
              console.log(part);
            }
          }
        );
      } else {
        const blobName = getBlobName(mesh.name);
        const stingBuffer = mesh.thumbnail.split("/");
        const thumbnail = stingBuffer[stingBuffer.length - 1];
        const blobThumb = getBlobName(thumbnail);
        blobService.createBlockBlobFromLocalFile('partlibrary', blobName, mesh.filePath, (error, stlResult) => {
          if (error) {
            // file uploaded
            console.log(error);
          } else {
            const stlPath = `https://bfmblob.blob.core.windows.net/partlibrary/${stlResult.name}`;
            console.log(mesh.thumbnailUploadPath);
            blobService.createBlockBlobFromLocalFile('partlibrary', blobThumb, mesh.thumbnailUploadPath, (error, thumbnailResult) => {
              if (error) {
                // file uploaded
                console.log(error);
              } else {
                const thumbnailPath = `https://bfmblob.blob.core.windows.net/partlibrary/${thumbnailResult.name}`;
                const newStl = new Stl({
                  name: mesh.name,
                  azureURL: stlPath,
                  thumbnail: thumbnailPath,
                  md5: mesh.md5,
                  position: mesh.position,
                  partType: mesh.partType,
                  partId: [partId]
                });
                newStl.save()
                  .then(stl => {
                    //console.log(stl);
                    const { _id } = stl;
                    Part.findByIdAndUpdate(
                      { _id: partId },
                      { $push: { meshes: _id } },
                      (err, result) => {
                        if (err) {
                          console.log(err)
                        } else {
                          console.log(result);
                        }
                      }
                    );
                  })
                  .catch(err => console.log(err));
              }
            })
          }
        });
      }
    });
  });
});

router.post("/createpartapp", async (req, res) => {
  const {
    partAppName,
    email,
    adminPass,
    partAppData
  } = req.body;

  PartsApp.findOne({ name: partAppName }).then(result => {
    if (result) {
      return res.status(200).json({ error: "Name already exisits." });
    }
  });
  User.findOne({ email }).then(user => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }

    // Check password
    bcrypt.compare(adminPass, user.password).then(isMatch => {
      if (isMatch) {
        const partApp = new PartsApp({
          name: partAppName,
          email: email,
          partsAppData: partAppData,
          active: true
        });
        partApp.save()
          .then(data => {
            console.log(data);
            res.status(200).json({ success: "Created Successfully" });
          })
          .catch(err => console.log(err));

      } else {
        return res
          .status(200)
          .json({ error: "Password incorrect" });
      }
    });
  });
});

router.get("/parts", (req, res) => {

  Part.find({}, (err, result) => {
    //console.log(result);
    res.json(result);
  });
});
router.get("/partapp", (req, res) => {

  PartsApp.find({}, (err, result) => {
    //console.log(result);
    res.json(result);
  });
});

router.put("/partapp", (req, res) => {
  const { email, name, active } = req.body;
  PartsApp.findOne({
    email: email,
    name: name
  }).then(part => {
    if (part == null) {
      res.status(403).send('Error');
    } else if (part != null) {
      part.active = active;
      part
        .save()
        .then(part => {
          res.status(200).send({ message: 'Data Changed' });
        })
        .catch(err => console.log(err));

    } else {
      console.error('no user exists in db to update');
      res.status(401).json('no user exists in db to update');
    }
  });
});

router.get("/stl/:partId", async (req, res) => {
  const { partId } = req.params;
  let partData = {};
  await Part.findOne({ _id: partId }, (err, result) => {
    partData.name = result.name;
    partData.tags = result.tags;
    partData.x = result.x;
    partData.y = result.y;
    partData.z = result.z;
    partData.partsAppName = result.partsAppName;
    partData.partsAppData = result.partsAppData;
    //console.log(partData);
  });
  //await Stl.find({ partId }, (err, result) => {
  await Stl.find({}, (err, results) => {
    const correctResult = results.filter(result => result.partId.includes(partId));
    partData.meshData = correctResult;
    res.json(partData);
  });
});

module.exports = router;
